package fr.umfds.ter;

public class Sujet {
	private int id;
	private Groupe groupe;
	private Enseignant rapporteur;
	private String titre;
	private String resume;
//constructeur parametré
	Sujet(int id){
		this.id = id;
	}
//id
	public int getId() {
		return id;
	}
	public void setId(int i) {
		id = i;
	}
//groupe
	public Groupe getGroupe() {
		return groupe;
	}
	public void setGroupe(Groupe g) {
		groupe = g;
	}
//rapporteur
	public Enseignant getRapporteur() {
		return rapporteur;
	}
	public void setRapporteur(Enseignant r) {
		rapporteur = r;
	}
//TITRE
	public String getTitre() {
		return titre;
	}
	public void setTitre(String t) {
		titre = t;
	}
//RESME
	public String getResume() {
		return resume;
	}
	public void setResume(String r) {
		resume = r;
	}
//METHODES
	public void affectationRapporteur(Enseignant rapporteur) {
		if(rapporteur.getSujets().size() > rapporteur.getGroupes().size() && !rapporteur.encadre(groupe) ) {
			this.rapporteur = rapporteur;
		}
	}
	
	
}
