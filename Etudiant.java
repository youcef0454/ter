package fr.umfds.ter;

public class Etudiant {
	private String nom;
	private String prenom;
//CONSTRUCTEUR
	Etudiant(String n, String p){
		this.nom = n;
		this.prenom = p;
	}
//nom
	public String getNom() {
		return nom;
	}
//prenom
	public String getPrenom() {
		return prenom;
	}
}
