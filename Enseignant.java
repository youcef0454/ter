package fr.umfds.ter;
import java.util.List;

public class Enseignant {
	private String nom;
	private String prenom;
	private List<Sujet> sujets;
	private List<Groupe> groupes;
	
//CONSTRUCTEUR
	Enseignant(String n, String p){
		this.nom = n;
		this.prenom = p;
	}
//NOM
	public String getNom() {
		return nom;
	}
//PRENOM
	public String getPrenom() {
		return prenom;
	}
//LISTESUJETS
	public List<Sujet> getSujets(){
		return sujets;
	}
	public void ajoutSujet(List<Sujet> s) {
		sujets =s;
	}
	
//LISTEGROUPES
	
	public List<Groupe> getGroupes(){
		return groupes;
	}
	public void ajouterGroupe(Groupe g) {
		this.groupes.add(g);
	}

//METHODES
	

	public boolean encadre(Groupe g) {
		boolean enc = false;
		int i=0;
		int nbGroupes = groupes.size();
		while(enc==false && i<nbGroupes) {
			enc = (g == groupes.get(i));
		}
		return enc;
	}
	
	
}
