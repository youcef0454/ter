package fr.umfds.ter;
import java.util.List;
public class Groupe {
	private List<Etudiant> membres; //contrainte : 5 membres max
	private Enseignant encadrant;
	private Enseignant rapporteur;
	private List<Sujet> voeux;//contrainte 1< x <6
	private Sujet sujet;
//CONSTRUCTEUR
	Groupe(List<Etudiant> membres, Enseignant encadrant ,Enseignant rapporteur){
		if(membres.size() > 5 || membres.size() < 0) {
			this.membres = membres ;
			encadrant.ajouterGroupe(this); //recoit ce groupe
		}
		this.encadrant = encadrant;
		this.rapporteur = rapporteur;
	}
//ENCADRANT
	public Enseignant getEncadrant() {
		return this.encadrant;
	}
	public void setEncadrant(Enseignant e) {
		encadrant = e;
	}
//RAPPORTEUR
	public Enseignant getRapporteur() {
		return this.rapporteur;
	}
	public void setRapporteur(Enseignant e) {
		rapporteur = e;
	}
//VOEUX
	public void formulerVoeux(Sujet s) {
		if(voeux.size()<6) {
			this.voeux.add(s) ;
		}
	}
	public List<Sujet> getVoeux(){
		return voeux;
	}
//SUJET
	
	public void affecterSujet(Sujet s) {
		this.sujet = s;
		s.setGroupe(this);		
	}
	public Sujet getSujet() {
		return sujet;
	}
	
	
	
	
	
}
